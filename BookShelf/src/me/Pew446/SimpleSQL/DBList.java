/**
@author Mitch Talmadge (AKA Pew446)

Date Created:
	May 12, 2013
	
Purpose:
	An enum of all the databases being used in Simple SQL
*/

package me.Pew446.SimpleSQL;

	public enum DBList
	{
		MySQL,
		SQLite
	}
